package ru.vartanyan.tm.command.user;

import org.jetbrains.annotations.NotNull;
import ru.vartanyan.tm.command.AbstractAuthCommand;
import ru.vartanyan.tm.command.AbstractUserCommand;
import ru.vartanyan.tm.enumerated.Role;
import ru.vartanyan.tm.util.TerminalUtil;

public class UserUnlockByIdCommand extends AbstractUserCommand {

    @Override
    public String arg() {
        return null;
    }

    @Override
    public @NotNull String name() {
        return "user-unlock-by-id";
    }

    @Override
    public String description() {
        return "Unlock user by Id";
    }

    @Override
    public void execute() throws Exception {
        System.out.println("[UNLOCK USER BY ID]");
        System.out.println("[ENTER ID]");
        @NotNull final String id = TerminalUtil.nextLine();
        serviceLocator.getUserService().unlockUserById(id);
    }

    @Override
    public Role[] roles() {
        return new Role[] {
                Role.ADMIN
        };
    }

}
