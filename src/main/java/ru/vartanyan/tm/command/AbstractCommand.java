package ru.vartanyan.tm.command;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.vartanyan.tm.api.service.ServiceLocator;
import ru.vartanyan.tm.enumerated.Role;

public abstract class AbstractCommand {

    protected ServiceLocator serviceLocator;

    public AbstractCommand() {
    }

    public void setServiceLocator(@NotNull final ServiceLocator serviceLocator) {
        this.serviceLocator = serviceLocator;
    }

    @Nullable public abstract String arg();

    @NotNull public abstract String name();

    @Nullable public abstract String description();

    public abstract void execute() throws Exception;

    @Nullable public Role[] roles() {
        return null;
    }

}
