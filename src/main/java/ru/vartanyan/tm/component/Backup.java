package ru.vartanyan.tm.component;

import lombok.SneakyThrows;
import ru.vartanyan.tm.bootstrap.Bootstrap;
import ru.vartanyan.tm.command.data.BackupLoadCommand;

import java.util.concurrent.Executor;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;

import static ru.vartanyan.tm.command.data.AbstractDataCommand.BACKUP_SAVE;
import static ru.vartanyan.tm.command.data.AbstractDataCommand.BACKUP_LOAD;

public class Backup implements Runnable {

    private final ScheduledExecutorService es = Executors.newSingleThreadScheduledExecutor();

    private static final int INTERVAL = 3;

    private final Bootstrap bootstrap;

    public Backup(Bootstrap bootstrap) {
        this.bootstrap = bootstrap;
    }

    @SneakyThrows
    public void run() {
        bootstrap.parseCommand(BACKUP_SAVE);
    }

    @SneakyThrows
    public void load() {
        bootstrap.parseCommand(BACKUP_LOAD);
    }

    public void init() {
        load();
        start();
    }

    public void start() {
        es.scheduleWithFixedDelay(this, 0, INTERVAL, TimeUnit.SECONDS);
    }

    public void stop() {
        es.shutdown();
    }

}
